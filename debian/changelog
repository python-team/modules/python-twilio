python-twilio (9.4.1-1) unstable; urgency=medium

  * Team upload.
  * Remove dependency on python3-mock

 -- Alexandre Detiste <tchet@debian.org>  Sun, 15 Dec 2024 02:30:38 +0100

python-twilio (9.0.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 9.0.5
  * depends on new python3-aiounittest, drop patch

 -- Alexandre Detiste <tchet@debian.org>  Sat, 27 Apr 2024 03:08:58 +0200

python-twilio (9.0.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 9.0.2
  * use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Thu, 21 Mar 2024 21:49:24 +0100

python-twilio (8.12.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 8.12.0
  * Fix and enable tests as much as possible (closes:#1058428):
    + patch: use unittest.IsolatedAsyncioTestCase
    + B-D python3-aiohttp-retry, drop w/a patch
    * disable tests that rely on internet
  * Use autopkgtest-pkg-pybuild
  * Update copyright
  * Drop dependency on python3-tz
  * Standards-Version 4.6.2
  * copyright: remove unused file exclusion

  [ Sébastien Delafond ]
  * d/control: remove Freexian team members from Uploaders

 -- Jérémy Lal <kapouer@melix.org>  Tue, 30 Jan 2024 08:44:41 +0100

python-twilio (8.6.0-1) experimental; urgency=medium

  * Team upload.
  * Upload to experimental.

  [ Neil Williams ]
  * Add python3-pytest to autopkgtest.
  * Update deps for new version and autopkgtest.

  [ Utkarsh Gupta ]
  * New upstream version 8.6.0.
  * Add Build-Depends on aiohttp, django, and pygments.
  * Add patch to skip AysncIO retrying.
  * Disable tests as some libraries are not packaged.
    - pyngrok and aiounittest are not packaged.

 -- Utkarsh Gupta <utkarsh@debian.org>  Tue, 15 Aug 2023 00:53:53 +0530

python-twilio (7.7.1+ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Neil Williams <codehelp@debian.org>  Mon, 21 Mar 2022 08:29:43 +0000

python-twilio (7.1.0+ds-1) unstable; urgency=medium

  * Team upload

  * Fix dh_python depends (lintian error)
  * Add Salsa CI
  * Update standards version
  * New upstream release. (Closes: #997490)

 -- Neil Williams <codehelp@debian.org>  Mon, 15 Nov 2021 14:37:03 +0000

python-twilio (6.51.0+ds-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 6.8.2-2.
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sophie Brun ]
  * New upstream version 6.51.0
  * Update debian/copyright
  * Repack upstream tarball to remove pre-built docs
  * Bump Standards-Version to 4.5.1 (no changes)

 -- Sophie Brun <sophie@freexian.com>  Thu, 21 Jan 2021 10:59:12 +0100

python-twilio (6.8.2-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Tue, 27 Aug 2019 08:03:44 +0200

python-twilio (6.8.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Chris Lamb ]
  * Remove new-package-should-not-package-python2-module override as it will be
    marked as 'unused' in the next release (and should have been mentioned in
    the changelog).

  [ Sébastien Delafond ]
  * Add myself to Uploaders
  * Add requests to dependencies (Closes: #917001)

 -- Sebastien Delafond <seb@debian.org>  Sun, 30 Dec 2018 18:52:12 +0100

python-twilio (6.8.2-1) unstable; urgency=medium

  * Initial release (Closes: #880184)

 -- Sophie Brun <sophie@freexian.com>  Mon, 30 Oct 2017 14:20:51 +0100
